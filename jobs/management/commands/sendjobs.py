################################################################################ 
# JobBoard: a simple Django-based job board
# Copyright (c) 2012, CandidOak Concepts
# All rights reserved.
#                    
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
################################################################################

from datetime import datetime, timedelta
from django.conf import settings
from django.core import mail
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand, CommandError
from django.template import Context
from django.template.loader import get_template
from django.utils import timezone
from jobs.models import JobPost, JobSubscriber


def get_jobposts_emails(subscribers, jobs):
    """
    Return a list of EmailMessage objects
    """
    
    plaintext = get_template('templated_email/job_postings_email.txt')
    htmly = get_template('templated_email/job_postings_email.html')    
    
    subject = "New Job Postings on " + settings.JOBBOARD_TITLE
    message = "Review the new job postings on our website."
    from_email = settings.JOBBOARD_FROM_EMAIL
            
    email_message_list = []
    for subscriber in subscribers:
        recipients = [subscriber.subscriber_email]
        email_params = Context({ 
            'name': subscriber.subscriber_email, 
            'jobs': jobs,
            'domain': settings.JOBBOARD_DOMAIN,
            'site_name': settings.JOBBOARD_TITLE
        })
        
        text_content = plaintext.render(email_params)
        html_content = htmly.render(email_params)
        
        email = EmailMultiAlternatives(subject, message, from_email, recipients)
        email.attach_alternative(html_content, "text/html")
        email_message_list.append( email )        
    
    return email_message_list

class Command(BaseCommand):
    args = ''
    help = 'Send jobs posted the previous day to active subscribers.'
    
    def handle(self, *args, **options):
        """
        Get jobs posted within the last specified days and send it to 
        subscribers.
        """
        
        days_ago = 2        
        jobs = JobPost.objects.filter( approved=True, date_posted__gte=timezone.localtime(timezone.now())-timedelta(days=days_ago) )
        subscribers = JobSubscriber.objects.filter( is_activated=True ) 
        
        if jobs.count() > 0:        
            connection = mail.get_connection() # Use default email connection
            messages = get_jobposts_emails(subscribers, jobs)
            connection.send_messages( messages )
            
            self.stdout.write('Successfully sent %d jobs to %d subscribers. \n' %  ( jobs.count(), subscribers.count() ) )
            
        else:
            self.stdout.write(str(datetime.now()) + ': No jobs to send. \n')
