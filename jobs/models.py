################################################################################ 
# JobBoard: a simple Django-based job board
# Copyright (c) 2012, CandidOak Concepts
# All rights reserved.
#                    
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
################################################################################

import datetime
from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

COUNTRY_CHOICES = (
    ('KE', 'Kenya'),
    ('TZ', 'Tanzania'),
    ('UG', 'Uganda'),
)

JOB_TYPE_CHOICES = (
    ('FULLTIME', 'Full Time'),
    ('PARTTIME', 'Part Time'),
)

################################################################################
## Models
################################################################################

class JobType(models.Model):
    """
    An available job type for working, be it a contract, full time or part time.

    These appear on drop-down forms for those submitting job posts.

    Field Attributes:
      name: The name of the job type
    """
    
    name = models.CharField(max_length=80)
    
    def __unicode__(self):
        return self.name
        

class JobCountry(models.Model):
    """
    An available job country for working, be it Kenya, Uganda or Tanzania.

    These appear on drop-down forms for those submitting job posts.

    Field Attributes:
      country_name: The name of the country
      short_desc: The short description of country i.e. KE, UG or TZ
    """
    
    country_name = models.CharField(max_length=60)
    short_desc = models.CharField(max_length=10)
    
    def __unicode__(self):
        return self.country_name


class JobPost(models.Model):
    """
    A post for an available job that people can apply to.

    These are usually user-submitted, and then approved by an
    administrator.  Once they are approved, they should show up on the
    job listing until the time they expire.

    Field Attributes:
        job_type:   Type of job i.e. Contract, Part time or full time.
        job_title:  Title of the job offered
        job_description:    Description of this job
        job_country:    Country where this job is offered
        company_name:   Name of the company posting the job
        company_url:    URL of the company website
        apply_information:  Information on how to apply for the job
        apply_email:    An email address relevant to this job (probably for 
                        contact purposes)
        date_posted:    The timestamp for when this post was submitted
        posted_by:  The user ID of the person or company posting the job
        approved:   Whether or not this application was approved to be
                    listed on the site.
        paid:       Whether or not this application has been paid for.
        post_anon:  Post anonymously
    """
    
    job_type = models.ForeignKey(JobType)
    job_title = models.CharField(max_length=200)
    job_description = models.TextField('Job Description')
    job_req = models.TextField('Job Requirements')
    job_country = models.ForeignKey(JobCountry)
    company_name = models.CharField(max_length=256)
    company_url = models.URLField()
    company_profile = models.TextField('Company Profile')
    apply_information = models.TextField('How to apply')
    apply_email = models.EmailField()
    job_deadline = models.DateField('Deadline Date')
    date_posted = models.DateTimeField('Date posted', auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    posted_by = models.ForeignKey(User)
    approved = models.BooleanField(default=False)
    paid = models.BooleanField(default=False)
    post_anon = models.BooleanField(default=False)
    slug = models.SlugField(max_length=200, blank=True, null=True, help_text='Unique value for job page url, created from job title.')
    
    class Meta:
        ordering = ['-date_posted']
        #unique_together = (('job_title', 'company_name'),)
    
    def __unicode__(self):
        return "%s - %s" % (self.company_name, self.job_title)
    
    @models.permalink    
    def get_absolute_url(self):
        return ('job_detail_view', (), {
            'job_id': self.id,
            'job_slug': self.slug,
        }) 
        
    def save(self, *args, **kwargs):
        if self.job_title:
            self.slug = slugify(self.job_title)
            super(JobPost, self).save(*args, **kwargs) # Call the "real" save() method.
        
        
class JobSubscriber(models.Model):
    """
    Subscriber who receives job alerts
    """
    subscriber_email = models.EmailField(max_length=254, unique=True)
    is_activated = models.BooleanField(default=False)
    key = models.CharField(max_length=32)
    
    def __unicode__(self):
        return self.subscriber_email
        
        
class JobTag(models.Model):
    """
    Tags used to categorize jobs
    """
    
    tag_name = models.CharField(max_length=30, unique=True)
    jobposts = models.ManyToManyField(JobPost)
    
    def __unicode__(self):
        return self.tag_name
        
    class Meta:
        ordering = ('tag_name',)
        

class JobPostView(models.Model):
    """
    Track the number of views per job post
    """
    
    jobpost = models.ForeignKey(JobPost, related_name='jobpostviews')
    ip = models.CharField(max_length=40)
    created = models.DateTimeField(default=datetime.datetime.now())
