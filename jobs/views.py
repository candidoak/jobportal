################################################################################ 
# JobBoard: a simple Django-based job board
# Copyright (c) 2012, CandidOak Concepts
# All rights reserved.
#                    
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
################################################################################

"""
Views for the JobBoard application.
"""

import datetime
import random
import string

from datetime import datetime, timedelta
from django import forms
from django.conf import settings
from django.core.mail import EmailMessage, EmailMultiAlternatives, send_mail
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.validators import email_re
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.template import Context
from django.template.loader import get_template
from django.utils import timezone
from django.utils.html import strip_tags

from jobs.models import JobType, JobCountry, JobPost, JobSubscriber, JobTag, JobPostView
from jobs.forms import ContactForm, JobPostForm, JobSubscriberForm

def index(request):
    """
    Index page.

    Displays a few job postings and a few applicant postings.
    """
    
    page_title = 'Tech Jobs in East Africa'
    days_ago = 31 # Show jobs posted within the last 30 days
        
    jobs_type_list = JobType.objects.all()
    
    job_type = request.GET.get('job_type')
    job_tag = request.GET.get('job_tag')
    job_country = request.GET.get('country')
    
    if job_type:
        jtype = JobType.objects.get(name=job_type)
        jobs_list = JobPost.objects.filter(approved=True, job_type=jtype, date_posted__gte=timezone.localtime(timezone.now())-timedelta(days=days_ago) )
        page_title = '%s Jobs' % jtype
    elif job_tag:
        jtag = JobTag.objects.get(tag_name=job_tag)
        jobs_list = jtag.jobposts.filter(approved=True, date_posted__gte=timezone.localtime(timezone.now())-timedelta(days=days_ago) )
        page_title = '%s Jobs' % jtag
    elif job_country:
        jcountry = JobCountry.objects.get(country_name=job_country)
        jobs_list = JobPost.objects.filter(approved=True, job_country=jcountry, date_posted__gte=timezone.localtime(timezone.now())-timedelta(days=days_ago) )
        page_title = 'Tech Jobs in %s' % jcountry
    else:
        jobs_list = JobPost.objects.filter(approved=True, date_posted__gte=timezone.localtime(timezone.now())-timedelta(days=days_ago) )
        
    paginator = Paginator(jobs_list, settings.JOBBOARD_JOBS_PER_PAGE) # Limit jobs per page
    
    page = request.GET.get('page')
    try:
        jobs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        jobs = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        jobs = paginator.page(paginator.num_pages)
    
    return render_to_response('index.html', { 
        'jobs':jobs, 
        'job_types':jobs_type_list,
        'page_title': page_title
        }, context_instance=RequestContext(request))
    
    
def signup(request):
     """
     Register a new user in the system
     """
     
     """ form = UserCreationForm
     
     if request.method == 'POST': # If the signup form has been submitted
        data = request.POST.copy()
        errors = form.get_validation_errors(data)
        if not errors:
            new_user = form.save(data)
            new_user.is_staff = False
            new_user.is_superuser = False
            return HttpResponseRedirect('/')
     else:
        data, errors = {}, {}
        
     return render_to_response( 'login_signup.html', {
        'form' : forms.FormWrapper(form, data, errors)
     }, context_instance=RequestContext(request) )"""
     
     error = False
     
     if request.method == 'POST': # If the signup form has been submitted
        username = request.POST['regusername']
        email = request.POST['regemail']
        password = request.POST['regpassword']
        #password2 = request.POST['regpassword']
        
        #if email_re.match(email):
            # if password == password2:
                # Do something here
        
        try:
            # Check that we have no current users with the same email
            user = User.objects.get(email=email)
            
            if user: # user exists              
                error = True
                error_msg = "That email address is already registered. Use another."    
                return render_to_response( 'signup.html', { 'error': error, 'error_msg': error_msg }, context_instance=RequestContext(request) )
        except User.DoesNotExist:
            
            # user does not exist   
            new_user = User.objects.create_user(username=username, email=email, password=password)
            new_user.is_staff = False
            new_user.is_superuser = False
            new_user.is_active = True
            new_user.save()
    
            new_user = auth.authenticate(username=username, password=password)
            if new_user is not None and new_user.is_active:
                auth.login(request, new_user)
                return HttpResponseRedirect('/user/home/')
            else:
                return HttpResponseRedirect('/signup/') 
                
     return render_to_response( 'signup.html', {}, context_instance=RequestContext(request) )
     
      
def login(request):
     """
     Open sesame
     """
     
     error = False
     
     if request.method == 'POST': # If the login form has been submitted
        email = request.POST['uemail']
        password = request.POST['password']
        
        user = auth.authenticate(email=email, password=password)
        if user is not None and user.is_active:
            auth.login(request, user)
            
            if user.is_staff: # This is a site admin
                return HttpResponseRedirect('/staff/home/')
            else: # This is a normal user
                return HttpResponseRedirect('/user/home/')
        else:
            error = True
            error_msg = "Email and password do not match. Try again."
            return HttpResponseRedirect('/login/')
        
     return render_to_response( 'login.html', {}, context_instance=RequestContext(request) )
     
     
def logout(request):
     """
     """
     auth.logout(request)
     #return render_to_response( 'logout.html', {}, context_instance=RequestContext(request) )
     return HttpResponseRedirect('/login/')
     

@login_required(login_url='/login/')     
def user_home(request):
    """
    The normal user home
    """
    if request.user.is_authenticated():
        jobs = JobPost.objects.filter(posted_by=request.user)
        return render_to_response( 'user/user_home.html', { 'jobs' : jobs }, context_instance=RequestContext(request) )
    
    # User is not authenticated. Go to login page
    return HttpResponseRedirect('/login/')  
    

@login_required(login_url='/login/')    
def staff_home(request):
    """
    The website admin home
    """
    if request.user.is_authenticated():
        if request.user.is_staff: # This is a site admin
            pending_jobs = JobPost.objects.filter(approved=False)
            return render_to_response( 'staff/staff_home.html', { 'pending_jobs' : pending_jobs }, context_instance=RequestContext(request) )
    
    # User is not authenticated. Go to login page
    return HttpResponseRedirect('/login/')


def job_detail_old(request, job_id):
    """
    Fetch and view a particular job
    """
    job = get_object_or_404(JobPost, pk=job_id)
    job_tags = JobTag.objects.filter(jobposts=job)
    
    # Has this job been viewed by current user?
    if not JobPostView.objects.filter(jobpost=job, ip=request.META['REMOTE_ADDR']):
        
        jobview = JobPostView(jobpost=job, ip=request.META['REMOTE_ADDR'], created=datetime.now())
        
        jobview.save()
        
    # Now get the total number of views for this job posting
    job_total_views = JobPostView.objects.filter(jobpost=job).count()
    
    return render_to_response( 'job_details.html', { 'job' : job, 'job_tags' : job_tags, 'job_views_count' : job_total_views }, context_instance=RequestContext(request) )
    

def job_detail(request, job_id, job_slug):
    """
    Fetch and view a particular job
    """
    job = get_object_or_404(JobPost, pk=job_id, slug=job_slug)
    job_tags = JobTag.objects.filter(jobposts=job)
    
    # Has this job been viewed by current user?
    if not JobPostView.objects.filter(jobpost=job, ip=request.META['REMOTE_ADDR']):
        
        jobview = JobPostView(jobpost=job, ip=request.META['REMOTE_ADDR'], created=datetime.now())
        
        jobview.save()
        
    # Now get the total number of views for this job posting
    job_total_views = JobPostView.objects.filter(jobpost=job).count()
    
    return render_to_response( 'job_details.html', { 'job' : job, 'job_tags' : job_tags, 'job_views_count' : job_total_views }, context_instance=RequestContext(request) )
    
    
@login_required(login_url='/login/')    
def user_job_detail(request, job_id):
    """
    Fetch and view a particular job, that belongs to this particular user 
    (Same as above but checks for user)
    """
    job = get_object_or_404(JobPost, pk=job_id, posted_by=request.user)
    job_tags = JobTag.objects.filter(jobposts=job)
    
    # Has this job been viewed by current user?
    if not JobPostView.objects.filter(jobpost=job, ip=request.META['REMOTE_ADDR']):
        
        jobview = JobPostView(jobpost=job, ip=request.META['REMOTE_ADDR'], created=datetime.now())
        
        jobview.save()
        
    # Now get the total number of views for this job posting
    job_total_views = JobPostView.objects.filter(jobpost=job).count()
    
    return render_to_response( 'user_job_details.html', { 'job' : job, 'job_tags' : job_tags, 'job_views_count' : job_total_views }, context_instance=RequestContext(request) )
    

@login_required(login_url='/login/')
def submit_job(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = JobPostForm(request.POST)
            
            if form.is_valid():
                new_job_post = form.save(commit=False)
                new_job_post.posted_by = request.user
                new_job_post.save()
                form.save_m2m()
                
                # Add tags to job
                for i in range(1, 10):
                    if request.POST['skills_value_%d' % i]:
                        skill_val = 'skills_value_%d' % i
                        skill_val = skill_val.strip()
                        
                        # Check if tag exists
                        tag = JobTag.objects.filter( tag_name=request.POST[skill_val] )
                        
                        # Tag does not exist
                        if not tag:
                            tag_new = JobTag( tag_name=request.POST[skill_val] )
                            tag_new.save()
                            tag_new.jobposts.add(new_job_post)
                                
                # Send email to administrator to approve
                subject = "New Job Posting"
                message = "A new job has been posted and needs your attention."
                sender = settings.JOBBOARD_FROM_EMAIL
                recipients = ['fraogongi@gmail.com', 'fraogongi@yahoo.co.uk'] # can be list or tuple if used with EmailMessage
                email = EmailMessage(subject, message, settings.JOBBOARD_FROM_EMAIL, recipients)
                email.send()
                
                return HttpResponseRedirect('/user/submit_job/')
            
        else:
            form = JobPostForm()
            
        return render_to_response('user/submit_job.html', {'form': form}, context_instance=RequestContext(request))
        
    # User is not authenticated. Go to login page
    return HttpResponseRedirect('/login/')


@login_required(login_url='/login/')
def update_job(request, job_id):
    if request.user.is_authenticated():
    
        job_post = JobPost.objects.get(pk=job_id, posted_by=request.user)
        
        if request.method == 'POST':
            form = JobPostForm(request.POST, instance=job_post)
            
            if form.is_valid():
                updated_job_post = form.save(commit=False)
                updated_job_post.approved = False
                updated_job_post.save()
                form.save_m2m()
                
                # Add tags to job
                """for i in range(1, 10):
                    if request.POST['skills_value_%d' % i]:
                        skill_val = 'skills_value_%d' % i
                        skill_val = skill_val.strip()
                        
                        # Check if tag exists
                        tag = JobTag.objects.filter( tag_name=request.POST[skill_val] )
                        
                        # Tag does not exist
                        if not tag:
                            tag_new = JobTag( tag_name=request.POST[skill_val] )
                            tag_new.save()
                            tag_new.jobposts.add(new_job_post)"""
                                
                # Send email to administrator to approve
                """subject = "New Updated Job Posting"
                message = "A job has been updated and needs your attention."
                sender = settings.JOBBOARD_FROM_EMAIL
                recipients = ['fraogongi@gmail.com', 'fraogongi@yahoo.co.uk'] # can be list or tuple if used with EmailMessage
                email = EmailMessage(subject, message, settings.JOBBOARD_FROM_EMAIL, recipients)
                email.send()"""
                
                return HttpResponseRedirect('/user/submit_job/')
            
        else:
            if job_id:
                form = JobPostForm(instance=job_post)
            
        return render_to_response('user/submit_job.html', {'form': form}, context_instance=RequestContext(request))
        
    # User is not authenticated. Go to login page
    return HttpResponseRedirect('/login/')
         

@login_required(login_url='/login/')
def delete_job(request, job_id):
    if request.user.is_authenticated():
        job_post = get_object_or_404(JobPost, pk=job_id)
        job_post.delete()
        return HttpResponseRedirect('/user/home/')
    
    # User is not authenticated. Go to login page
    return HttpResponseRedirect('/login/')
    
    
@login_required(login_url='/login/')    
def approve_job(request, job_id):
    """
    Approve a Job listing. Only the Admin can do this.
    """
    if request.user.is_authenticated():
        if request.user.is_staff: # This is a site admin
            job = get_object_or_404(JobPost, pk=job_id)
            job.approved = True
            job.save()
            
            return HttpResponseRedirect('/staff/home/')
    
    # User is not authenticated. Go to login page
    return HttpResponseRedirect('/login/')
    
    
def job_alert(request):
    """
    Setting up subscriber job alerts
    """
    
    if request.method == 'POST':
        form = JobSubscriberForm(request.POST)
        
        if form.is_valid():
            c_data = form.cleaned_data
               
            sub_email = c_data['subscriber_email']
            
            # Ensure that the email does not exist in the database
            subscriber = JobSubscriber.objects.filter(subscriber_email=sub_email)
            
            if not subscriber:
            
                new_subscriber = form.save(commit=False)
                new_subscriber.is_activated = False
                new_subscriber.key = ''.join(random.choice(string.letters) for i in xrange(32)) # generate key which will be sent in email
                new_subscriber.save()
                form.save_m2m()
                
                plaintext = get_template('templated_email/subscription_confirmation_email.txt')
                htmly = get_template('templated_email/subscription_confirmation_email.html')
                
                email_params = Context({ 
                    'name': sub_email, 
                    'uid': new_subscriber.id, 
                    'key': new_subscriber.key, 
                    'domain': settings.JOBBOARD_DOMAIN, 
                    'site_name': settings.JOBBOARD_TITLE 
                })
                
                subject = settings.JOBBOARD_TITLE + ': Please Confirm Subscription' 
                from_email = settings.JOBBOARD_FROM_EMAIL
                to = sub_email
                
                text_content = plaintext.render(email_params)
                html_content = htmly.render(email_params)
                
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                            
                return HttpResponseRedirect('/jobs/alert/thanks/')
        
    else:
        form = JobSubscriberForm()
        
    return render_to_response( 'job_alert.html', {'form': form}, context_instance=RequestContext(request) )
    

def job_alert_subscribe_confirm(request):
    """
    Confirm job alert subscription
    """
    
    sub_id = request.GET.get('u')
    sub_key = request.GET.get('k')
    
    if sub_id and sub_key:
        subscriber = get_object_or_404(JobSubscriber, pk=sub_id)
        if (subscriber.key == strip_tags(sub_key)) and (subscriber.is_activated is False):
            subscriber.is_activated = True
            subscriber.save()
            return HttpResponseRedirect('/jobs/alert/thanks/confirm/')
            
    form = JobSubscriberForm()
            
    return render_to_response( 'job_alert.html', {'form': form}, context_instance=RequestContext(request) )


def job_alert_confirm_thanks(request):
    """
    Thank you page after confirming subscription
    """
    return render_to_response( 'job_alert_thanks_confirmation.html', {}, context_instance=RequestContext(request) )
    

def job_alert_thanks(request):
    """
    Thank you page after subscribing
    """
    return render_to_response( 'job_alert_thanks.html', {}, context_instance=RequestContext(request) )
    
        
def view_subscribers(request):
    """
    The list of subscribers
    """
    if request.user.is_authenticated():
        if request.user.is_staff: # This is a site admin. OK
        
            sub_action = request.GET.get('action')
            sub_id = request.GET.get('sub_id')
            
            subscribers_list = None
            
            # delete if action and id is passed
            if sub_action and sub_id:
                subscriber = get_object_or_404(JobSubscriber, pk=sub_id)
                subscriber.delete()
                
                subscribers_list = JobSubscriber.objects.all()
                return HttpResponseRedirect('/staff/subscribers/')
            else:
                subscribers_list = JobSubscriber.objects.all()
            
            return render_to_response( 'staff/subscriber_list.html', { 'subscribers' : subscribers_list }, context_instance=RequestContext(request) )
    
    # User is not authenticated. Go to login page
    return HttpResponseRedirect('/login/')
    
    
def search_jobs(request):
    """
    Search for jobs matching the given criteria
    """
    
    search_term = None
    
    if 's' in request.GET and request.GET['s']:
        search_term = request.GET['s']
        jobs_list = JobPost.objects.filter(job_title__icontains=search_term)
        
        paginator = Paginator(jobs_list, settings.JOBBOARD_JOBS_PER_PAGE) # Limit jobs per page
        
        jobs = None
        page = request.GET.get('page')
        try:
            jobs = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            jobs = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            jobs = paginator.page(paginator.num_pages)        

        return render_to_response('search_results.html', {'jobs': jobs, 'query': search_term}, context_instance=RequestContext(request) )
    else:
        error_msg = "Please submit a search term."
        return render_to_response('search_results.html', {'error_msg': error_msg, 'query': search_term}, context_instance=RequestContext(request) )
        

def contact(request):
    """
    Contact form
    """
    
    if request.method == 'POST':
        form = ContactForm(request.POST)
        
        if form.is_valid():
            c_data = form.cleaned_data
            
            msg = EmailMultiAlternatives(
                c_data['subject'],
                strip_tags(c_data['message']),
                c_data.get('email', 'fraogongi@yahoo.co.uk'),
                ['fraogongi@gmail.com'],
            )
            msg.attach_alternative(c_data['message'], "text/html")
            msg.send()
            
            return HttpResponseRedirect('/contact/thanks/')
    else:
        form = ContactForm(
            initial = {'subject': 'TechJobs Query!'}
        )
    return render_to_response('contact_form.html', {'form': form}, context_instance=RequestContext(request))


def contact_thanks(request):
    """
    Thank you page after sending contact us form
    """
    return render_to_response( 'contact_thanks.html', {}, context_instance=RequestContext(request) )
 
    
def terms_conditions(request):
    """
    Terms and Conditions page

    """
    return render_to_response( 'terms_and_conditions.html', {}, context_instance=RequestContext(request) )
