################################################################################ 
# JobBoard: a simple Django-based job board
# Copyright (c) 2012, CandidOak Concepts
# All rights reserved.
#                    
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
################################################################################

from django.conf.urls import patterns, include, url
from jobs.feeds import LatestJobsFeed
from jobs.sitemap import JobPostSitemap
from django.contrib.sitemaps.views import sitemap

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

sitemaps = {
    'jobs': JobPostSitemap,
}

urlpatterns = patterns('jobs.views',
    url(r'^$', 'index', {}, 'home_page'),
    url(r'^jobs/$', 'index', {}, 'jobs_home_page'),
    url(r'^jobs/view/(?P<job_id>\d+)/$', 'job_detail_old', {}),
    url(r'^jobs/(?P<job_id>\d+)/(?P<job_slug>[-\w]+)/$', 'job_detail', {}, 'job_detail_view'),
    url(r'^jobs/approve/(?P<job_id>\d+)/$', 'approve_job', {}),
    url(r'^jobs/alert/$', 'job_alert', {}),
    url(r'^jobs/alert/subscribe/confirm/$', 'job_alert_subscribe_confirm', {}),
    url(r'^jobs/alert/thanks/confirm/$', 'job_alert_confirm_thanks', {}),
    url(r'^jobs/alert/thanks/$', 'job_alert_thanks', {}),
    url(r'^jobs/search/$', 'search_jobs', {}),
    
    url(r'^login/$', 'login', {}),
    url(r'^logout/$', 'logout', {}),
    url(r'^signup/$', 'signup', {}),
    
    url(r'^contact/$', 'contact', {}),
    url(r'^contact/thanks/$', 'contact_thanks', {}),
    
    url(r'^user/home/$', 'user_home', {}),
    url(r'^user/submit_job/$', 'submit_job', {}),
    url(r'^user/jobs/edit/(?P<job_id>\d+)/$', 'update_job', {}),
    url(r'^user/jobs/view/(?P<job_id>\d+)/$', 'user_job_detail', {}),
    url(r'^user/jobs/delete/(?P<job_id>\d+)/$', 'delete_job', {}),
    
    url(r'^staff/home/$', 'staff_home', {}),
    url(r'^staff/subscribers/$', 'view_subscribers', {}),
    
    url(r'^terms_and_conditions/$', 'terms_conditions', {}),
    
    url(r'^feeds/jobs/latest/$', LatestJobsFeed()),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}),    
)
