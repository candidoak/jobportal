from jobs.models import JobCountry, JobPost

from django import template
register = template.Library()


@register.inclusion_tag('country_no_jobs.html', takes_context=True)
def show_country_job_count(context):
    """
    Get the number of tech jobs in each country
    """
    
    country_jobs = dict() # No. of jobs per country
    countries = JobCountry.objects.all()
    for country in countries:        
        country_jobs[country.country_name] = JobPost.objects.filter(job_country=country, approved=True).count()
    return { "country_jobs" : country_jobs }
